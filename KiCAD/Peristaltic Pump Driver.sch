EESchema Schematic File Version 4
LIBS:Peristaltic Pump Driver-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PumpUNO"
Date "lun. 30 mars 2015"
Rev "V1.0"
Comp "FORTH - IESL - ElectroLab"
Comment1 ""
Comment2 "CC-By-SA 4.0"
Comment3 ""
Comment4 "Copyright (c) 2020 by Jann Kruse <jkruse@iesl.forth.gr>"
$EndDescr
Text Label 8950 1450 1    60   ~ 0
Vin
Text Label 9350 1450 1    60   ~ 0
IOREF
Text Label 8900 2500 0    60   ~ 0
A0
Text Label 8900 2600 0    60   ~ 0
A1
Text Label 8900 2700 0    60   ~ 0
A2
Text Label 8900 2800 0    60   ~ 0
A3
Text Label 8900 2900 0    60   ~ 0
A4(SDA)
Text Label 8900 3000 0    60   ~ 0
A5(SCL)
Text Label 10550 3000 0    60   ~ 0
0(Rx)
Text Label 10550 2800 0    60   ~ 0
2
Text Label 10550 2900 0    60   ~ 0
1(Tx)
Text Label 10550 2700 0    60   ~ 0
3(**)
Text Label 10550 2600 0    60   ~ 0
4
Text Label 10550 2500 0    60   ~ 0
5(*)
Text Label 10550 2400 0    60   ~ 0
6(*)
Text Label 10550 2300 0    60   ~ 0
7
Text Label 10550 2100 0    60   ~ 0
8
Text Label 10550 2000 0    60   ~ 0
9(**)
Text Label 10550 1900 0    60   ~ 0
10(**/SS)
Text Label 10550 1800 0    60   ~ 0
11(**/MOSI)
Text Label 10550 1700 0    60   ~ 0
12(MISO)
Text Label 10550 1600 0    60   ~ 0
13(SCK)
Text Label 10550 1400 0    60   ~ 0
AREF
NoConn ~ 9400 1600
Text Label 10550 1300 0    60   ~ 0
A4(SDA)
Text Label 10550 1200 0    60   ~ 0
A5(SCL)
Text Notes 10850 1000 0    60   ~ 0
Holes
Text Notes 8550 750  0    60   ~ 0
Shield for Arduino that uses\nthe same pin disposition\nlike "Uno" board Rev 3.
$Comp
L Connector_Generic:Conn_01x08 P1
U 1 1 56D70129
P 9600 1900
F 0 "P1" H 9600 2350 50  0000 C CNN
F 1 "Power" V 9700 1900 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" V 9750 1900 20  0000 C CNN
F 3 "" H 9600 1900 50  0000 C CNN
	1    9600 1900
	1    0    0    -1  
$EndComp
Text Label 8650 1800 0    60   ~ 0
Reset
$Comp
L power:+3.3V #PWR01
U 1 1 56D70538
P 9150 1450
F 0 "#PWR01" H 9150 1300 50  0001 C CNN
F 1 "+3.3V" V 9150 1700 50  0000 C CNN
F 2 "" H 9150 1450 50  0000 C CNN
F 3 "" H 9150 1450 50  0000 C CNN
	1    9150 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 56D707BB
P 9050 1350
F 0 "#PWR02" H 9050 1200 50  0001 C CNN
F 1 "+5V" V 9050 1550 50  0000 C CNN
F 2 "" H 9050 1350 50  0000 C CNN
F 3 "" H 9050 1350 50  0000 C CNN
	1    9050 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 56D70CC2
P 9300 3150
F 0 "#PWR03" H 9300 2900 50  0001 C CNN
F 1 "GND" H 9300 3000 50  0000 C CNN
F 2 "" H 9300 3150 50  0000 C CNN
F 3 "" H 9300 3150 50  0000 C CNN
	1    9300 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 56D70CFF
P 10350 1500
F 0 "#PWR04" H 10350 1250 50  0001 C CNN
F 1 "GND" H 10350 1350 50  0000 C CNN
F 2 "" H 10350 1500 50  0000 C CNN
F 3 "" H 10350 1500 50  0000 C CNN
	1    10350 1500
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 P2
U 1 1 56D70DD8
P 9600 2700
F 0 "P2" H 9600 2300 50  0000 C CNN
F 1 "Analog" V 9700 2700 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x06" V 9750 2750 20  0000 C CNN
F 3 "" H 9600 2700 50  0000 C CNN
	1    9600 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P5
U 1 1 56D71177
P 10800 650
F 0 "P5" V 10900 650 50  0000 C CNN
F 1 "CONN_01X01" V 10900 650 50  0001 C CNN
F 2 "Socket_Arduino_Uno:Arduino_1pin" H 10721 724 20  0000 C CNN
F 3 "" H 10800 650 50  0000 C CNN
	1    10800 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P6
U 1 1 56D71274
P 10900 650
F 0 "P6" V 11000 650 50  0000 C CNN
F 1 "CONN_01X01" V 11000 650 50  0001 C CNN
F 2 "Socket_Arduino_Uno:Arduino_1pin" H 10900 650 20  0001 C CNN
F 3 "" H 10900 650 50  0000 C CNN
	1    10900 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P7
U 1 1 56D712A8
P 11000 650
F 0 "P7" V 11100 650 50  0000 C CNN
F 1 "CONN_01X01" V 11100 650 50  0001 C CNN
F 2 "Socket_Arduino_Uno:Arduino_1pin" V 11000 650 20  0001 C CNN
F 3 "" H 11000 650 50  0000 C CNN
	1    11000 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P8
U 1 1 56D712DB
P 11100 650
F 0 "P8" V 11200 650 50  0000 C CNN
F 1 "CONN_01X01" V 11200 650 50  0001 C CNN
F 2 "Socket_Arduino_Uno:Arduino_1pin" H 11024 572 20  0000 C CNN
F 3 "" H 11100 650 50  0000 C CNN
	1    11100 650 
	0    -1   -1   0   
$EndComp
NoConn ~ 10800 850 
NoConn ~ 10900 850 
NoConn ~ 11000 850 
NoConn ~ 11100 850 
$Comp
L Connector_Generic:Conn_01x08 P4
U 1 1 56D7164F
P 10000 2600
F 0 "P4" H 10000 2100 50  0000 C CNN
F 1 "Digital" V 10100 2600 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" V 10150 2550 20  0000 C CNN
F 3 "" H 10000 2600 50  0000 C CNN
	1    10000 2600
	-1   0    0    -1  
$EndComp
Wire Notes Line
	8525 825  9925 825 
Wire Notes Line
	9925 825  9925 475 
Wire Wire Line
	9350 1450 9350 1700
Wire Wire Line
	9350 1700 9400 1700
Wire Wire Line
	9400 1900 9150 1900
Wire Wire Line
	9400 2000 9050 2000
Wire Wire Line
	9400 2300 8950 2300
Wire Wire Line
	9400 2100 9300 2100
Wire Wire Line
	9400 2200 9300 2200
Connection ~ 9300 2200
Wire Wire Line
	9050 2000 9050 1350
Wire Wire Line
	9150 1900 9150 1450
Wire Wire Line
	9400 2500 8900 2500
Wire Wire Line
	9400 2600 8900 2600
Wire Wire Line
	9400 2700 8900 2700
Wire Wire Line
	9400 2800 8900 2800
Wire Wire Line
	9400 2900 8900 2900
Wire Wire Line
	9400 3000 8900 3000
$Comp
L Connector_Generic:Conn_01x10 P3
U 1 1 56D721E0
P 10000 1600
F 0 "P3" H 10000 2150 50  0000 C CNN
F 1 "Digital" V 10100 1600 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x10" V 10150 1600 20  0000 C CNN
F 3 "" H 10000 1600 50  0000 C CNN
	1    10000 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10200 2100 10550 2100
Wire Wire Line
	10200 2000 10550 2000
Wire Wire Line
	10200 1900 10550 1900
Wire Wire Line
	10200 1800 10550 1800
Wire Wire Line
	10200 1700 10550 1700
Wire Wire Line
	10200 1600 10550 1600
Wire Wire Line
	10200 1400 10550 1400
Wire Wire Line
	10200 3000 10550 3000
Wire Wire Line
	10200 2900 10550 2900
Wire Wire Line
	10200 2800 10550 2800
Wire Wire Line
	10200 2700 10550 2700
Wire Wire Line
	10200 2600 10550 2600
Wire Wire Line
	10200 2500 10550 2500
Wire Wire Line
	10200 2400 10550 2400
Wire Wire Line
	10200 2300 10550 2300
Wire Wire Line
	9300 2100 9300 2200
Wire Wire Line
	9300 2200 9300 3150
Wire Notes Line
	8500 500  8500 3450
Wire Notes Line
	8500 3450 11200 3450
Wire Wire Line
	9400 1800 8650 1800
Text Notes 9700 1600 0    60   ~ 0
1
Wire Notes Line
	11200 1000 10700 1000
Wire Notes Line
	10700 1000 10700 500 
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5F16D8CE
P 4300 3100
F 0 "J1" H 4220 3317 50  0000 C CNN
F 1 "Pump1" H 4220 3226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4300 3100 50  0001 C CNN
F 3 "~" H 4300 3100 50  0001 C CNN
	1    4300 3100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5F16D947
P 4300 3650
F 0 "J2" H 4220 3867 50  0000 C CNN
F 1 "Pump2" H 4220 3776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4300 3650 50  0001 C CNN
F 3 "~" H 4300 3650 50  0001 C CNN
	1    4300 3650
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5F16D977
P 4300 4150
F 0 "J3" H 4220 4367 50  0000 C CNN
F 1 "Pump3" H 4220 4276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4300 4150 50  0001 C CNN
F 3 "~" H 4300 4150 50  0001 C CNN
	1    4300 4150
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5F16D9A7
P 4300 4700
F 0 "J4" H 4220 4917 50  0000 C CNN
F 1 "Pump4" H 4220 4826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4300 4700 50  0001 C CNN
F 3 "~" H 4300 4700 50  0001 C CNN
	1    4300 4700
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 5F16DC80
P 5800 3200
F 0 "J6" H 5700 3650 50  0000 C CNN
F 1 "Pololu713-I-Logic" V 5850 3150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5800 3200 50  0001 C CNN
F 3 "~" H 5800 3200 50  0001 C CNN
	1    5800 3200
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J5
U 1 1 5F16DE8B
P 5500 3200
F 0 "J5" H 5350 3650 50  0000 L CNN
F 1 "Pololu713-I-Power" V 5550 2750 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5500 3200 50  0001 C CNN
F 3 "~" H 5500 3200 50  0001 C CNN
	1    5500 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J8
U 1 1 5F16E2D1
P 5800 4500
F 0 "J8" H 5700 4950 50  0000 C CNN
F 1 "Pololu713-II-Logic" V 5850 4450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5800 4500 50  0001 C CNN
F 3 "~" H 5800 4500 50  0001 C CNN
	1    5800 4500
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J7
U 1 1 5F16E2D7
P 5500 4500
F 0 "J7" H 5350 4950 50  0000 L CNN
F 1 "Pololu713-II-Power" V 5550 4050 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5500 4500 50  0001 C CNN
F 3 "~" H 5500 4500 50  0001 C CNN
	1    5500 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F17351E
P 5200 2900
F 0 "#PWR0101" H 5200 2650 50  0001 C CNN
F 1 "GND" V 5205 2772 50  0000 R CNN
F 2 "" H 5200 2900 50  0001 C CNN
F 3 "" H 5200 2900 50  0001 C CNN
	1    5200 2900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F173560
P 5200 3600
F 0 "#PWR0102" H 5200 3350 50  0001 C CNN
F 1 "GND" V 5205 3472 50  0000 R CNN
F 2 "" H 5200 3600 50  0001 C CNN
F 3 "" H 5200 3600 50  0001 C CNN
	1    5200 3600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5F17358B
P 5200 4200
F 0 "#PWR0103" H 5200 3950 50  0001 C CNN
F 1 "GND" V 5205 4072 50  0000 R CNN
F 2 "" H 5200 4200 50  0001 C CNN
F 3 "" H 5200 4200 50  0001 C CNN
	1    5200 4200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F1735B6
P 5200 4900
F 0 "#PWR0104" H 5200 4650 50  0001 C CNN
F 1 "GND" V 5205 4772 50  0000 R CNN
F 2 "" H 5200 4900 50  0001 C CNN
F 3 "" H 5200 4900 50  0001 C CNN
	1    5200 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 2900 5300 2900
Wire Wire Line
	5200 3600 5300 3600
Wire Wire Line
	5200 4200 5300 4200
Wire Wire Line
	5200 4900 5300 4900
Wire Wire Line
	5200 3000 5300 3000
Wire Wire Line
	5300 4300 5200 4300
$Comp
L power:+12V #PWR0107
U 1 1 5F177EB8
P 8950 1250
F 0 "#PWR0107" H 8950 1100 50  0001 C CNN
F 1 "+12V" V 8950 1450 50  0000 C CNN
F 2 "" H 8950 1250 50  0001 C CNN
F 3 "" H 8950 1250 50  0001 C CNN
	1    8950 1250
	1    0    0    -1  
$EndComp
Text Label 5300 3500 2    60   ~ 0
Vin
$Comp
L power:+12V #PWR0108
U 1 1 5F17805D
P 5200 3500
F 0 "#PWR0108" H 5200 3350 50  0001 C CNN
F 1 "+12V" V 5215 3628 50  0000 L CNN
F 2 "" H 5200 3500 50  0001 C CNN
F 3 "" H 5200 3500 50  0001 C CNN
	1    5200 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 3500 5300 3500
Wire Wire Line
	8950 1250 8950 2300
Wire Wire Line
	5300 3100 4500 3100
Wire Wire Line
	5300 3200 4500 3200
Wire Wire Line
	5300 3400 4800 3400
Wire Wire Line
	4800 3400 4800 3650
Wire Wire Line
	4800 3650 4500 3650
Wire Wire Line
	5300 3300 4700 3300
Wire Wire Line
	4700 3300 4700 3550
Wire Wire Line
	4700 3550 4500 3550
$Comp
L power:+12V #PWR0109
U 1 1 5F188515
P 5200 4800
F 0 "#PWR0109" H 5200 4650 50  0001 C CNN
F 1 "+12V" V 5215 4928 50  0000 L CNN
F 2 "" H 5200 4800 50  0001 C CNN
F 3 "" H 5200 4800 50  0001 C CNN
	1    5200 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 4800 5300 4800
Text Label 5300 4800 2    60   ~ 0
Vin
Wire Wire Line
	5300 4700 4500 4700
Wire Wire Line
	5300 4600 4500 4600
Wire Wire Line
	5300 4400 4800 4400
Wire Wire Line
	4800 4400 4800 4150
Wire Wire Line
	4800 4150 4500 4150
Wire Wire Line
	4500 4250 4700 4250
Wire Wire Line
	4700 4250 4700 4500
Wire Wire Line
	4700 4500 5300 4500
Text Label 6400 3500 0    60   ~ 0
3(**)
Text Label 6400 2900 0    60   ~ 0
9(**)
Text Label 6400 4200 0    60   ~ 0
10(**/SS)
Text Label 6400 4800 0    60   ~ 0
11(**/MOSI)
$Comp
L power:GND #PWR0110
U 1 1 5F191689
P 6100 3600
F 0 "#PWR0110" H 6100 3350 50  0001 C CNN
F 1 "GND" V 6105 3472 50  0000 R CNN
F 2 "" H 6100 3600 50  0001 C CNN
F 3 "" H 6100 3600 50  0001 C CNN
	1    6100 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3600 6100 3600
Wire Wire Line
	6000 4900 6100 4900
Text Label 6400 3400 0    60   ~ 0
4
Text Label 6400 3300 0    60   ~ 0
5(*)
Text Label 6400 3200 0    60   ~ 0
6(*)
Text Label 6400 3100 0    60   ~ 0
7
Text Label 6400 3000 0    60   ~ 0
8
Text Label 6400 4700 0    60   ~ 0
A0
Text Label 6400 4600 0    60   ~ 0
A1
Text Label 6400 4500 0    60   ~ 0
A2
Text Label 6400 4400 0    60   ~ 0
A3
Text Label 6400 4300 0    60   ~ 0
A4(SDA)
Wire Wire Line
	6000 2900 6400 2900
Wire Wire Line
	6400 3000 6000 3000
Wire Wire Line
	6000 3100 6400 3100
Wire Wire Line
	6400 3200 6000 3200
Wire Wire Line
	6400 3300 6000 3300
Wire Wire Line
	6000 3400 6400 3400
Wire Wire Line
	6400 3500 6000 3500
$Comp
L power:+5V #PWR0105
U 1 1 5F296B6E
P 5200 4300
F 0 "#PWR0105" H 5200 4150 50  0001 C CNN
F 1 "+5V" V 5200 4500 50  0000 C CNN
F 2 "" H 5200 4300 50  0000 C CNN
F 3 "" H 5200 4300 50  0000 C CNN
	1    5200 4300
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5F296D4F
P 5200 3000
F 0 "#PWR0106" H 5200 2850 50  0001 C CNN
F 1 "+5V" V 5200 3200 50  0000 C CNN
F 2 "" H 5200 3000 50  0000 C CNN
F 3 "" H 5200 3000 50  0000 C CNN
	1    5200 3000
	0    -1   -1   0   
$EndComp
Text Label 6050 2900 0    50   ~ 0
PWMA_I
Text Label 6050 3000 0    50   ~ 0
AIN2_I
Text Label 6050 3100 0    50   ~ 0
AIN1_I
Text Label 6050 3200 0    50   ~ 0
-STBY_I
Text Label 6050 3300 0    50   ~ 0
BIN1_I
Text Label 6050 3400 0    50   ~ 0
BIN2_I
Text Label 6050 3500 0    50   ~ 0
PWMB_I
Wire Wire Line
	6000 4200 6400 4200
Wire Wire Line
	6400 4300 6000 4300
Wire Wire Line
	6000 4400 6400 4400
Wire Wire Line
	6400 4500 6000 4500
Wire Wire Line
	6400 4600 6000 4600
Wire Wire Line
	6000 4700 6400 4700
Wire Wire Line
	6400 4800 6000 4800
Text Label 6050 4200 0    50   ~ 0
PWMA_II
Text Label 6050 4300 0    50   ~ 0
AIN2_II
Text Label 6050 4400 0    50   ~ 0
AIN1_II
Text Label 6050 4500 0    50   ~ 0
-STBY_II
Text Label 6050 4600 0    50   ~ 0
BIN1_II
Text Label 6050 4700 0    50   ~ 0
BIN2_II
Text Label 6050 4800 0    50   ~ 0
PWMB_II
$Comp
L Connector_Generic:Conn_01x05 J9
U 1 1 5FA928D9
P 4300 2400
F 0 "J9" H 4220 1975 50  0000 C CNN
F 1 "UI" H 4220 2066 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-05A_1x05_P2.54mm_Vertical" H 4300 2400 50  0001 C CNN
F 3 "~" H 4300 2400 50  0001 C CNN
	1    4300 2400
	-1   0    0    1   
$EndComp
Text Label 4650 2200 0    60   ~ 0
A5(SCL)
Text Label 5150 2300 0    60   ~ 0
AREF
Text Label 4650 2500 0    60   ~ 0
13(SCK)
Text Label 4650 2600 0    60   ~ 0
12(MISO)
$Comp
L power:GND #PWR0111
U 1 1 5FA92B03
P 5150 2400
F 0 "#PWR0111" H 5150 2150 50  0001 C CNN
F 1 "GND" H 5150 2250 50  0000 C CNN
F 2 "" H 5150 2400 50  0000 C CNN
F 3 "" H 5150 2400 50  0000 C CNN
	1    5150 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4500 2200 4650 2200
Wire Wire Line
	4500 2300 5150 2300
Wire Wire Line
	5150 2400 4500 2400
Wire Wire Line
	4500 2500 4650 2500
Wire Wire Line
	4650 2600 4500 2600
Wire Wire Line
	10200 1500 10350 1500
Wire Wire Line
	10200 1200 10550 1200
Wire Wire Line
	10200 1300 10550 1300
$EndSCHEMATC
