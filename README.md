# Hardware

Here you can find the hardware design files.
The PCB is designed in KiCAD and the case is in STL format, which can be read by most 3D-printers and 3D design programs.

![Pump Driver Box with 3 Pumps](Pump%20Driver%20with%203%20Pumps.jpeg)